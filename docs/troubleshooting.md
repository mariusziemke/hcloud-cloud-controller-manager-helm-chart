# Troubleshooting

In case something does not work as expected, check if the symptom and possible solution is known:

## General

  * **coredns, network plugin pods and/or hcloud-cloud-controller-manager Pods do not start**
    When Kubernetes is installed with `--cloud-provider=external`, coredns and your network plugins may not start,
    since the nodes are not yet initialized by hcloud-cloud-controller-manager.
    In order to make the Hetzner API endpoint reachable for hcloud-cloud-controller-manager,
    you have to bring up coredns and your network.
    You can bring up coredns by adding a toleration to run on uninitialized nodes:
    ```
    kubectl -n kube-system patch deployment coredns --type json -p '[{"op":"add","path":"/spec/template/spec/tolerations/-","value":{"key":"node.cloudprovider.kubernetes.io/uninitialized","value":"true","effect":"NoSchedule"}}]'
    ```


## hcloud-cloud-controller-manager Logs

  * **failed to find kubelet node IP from cloud provider**:
    Double check, if the provided Hetzner API token (either configured in `secret.hcloudApiToken` or using an existing secret with `secret.existingSecretName`) is correct.
    Furthermore, if the node's main IP address is within a Hetzner private network range, configure `config.privateNetworks.id` and `config.privateNetworks.subnet` accordingly and enable private network support by setting `config.privateNetworks.enabled` to `true`.


If your symptom is not listed here or the proposed solution did not work,
please check if it is already a [known problem](https://gitlab.com/MatthiasLohr/hcloud-cloud-controller-manager-helm-chart/-/issues).
If this still does not help, feel free to [open an issue here](https://gitlab.com/MatthiasLohr/hcloud-cloud-controller-manager-helm-chart/-/issues/new).
